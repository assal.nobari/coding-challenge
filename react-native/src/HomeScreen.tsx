import { MaterialCommunityIcons } from "@expo/vector-icons";
import React, { useEffect, useState } from "react";
import {
  RefreshControl,
  ScrollView,
  StyleSheet,
  View,
  Dimensions
} from "react-native";
import { Appbar, FAB } from "react-native-paper";
import { useSelector, useDispatch } from "react-redux";
import { selectors, actions } from "./store/inventory";
import { RootState } from "./store";
import { SafeAreaView } from "react-native-safe-area-context";
import { StackScreenProps } from "@react-navigation/stack";
import InventoryList from "./Components/InventoryList";

export default (props: StackScreenProps<{}>) => {
  const fetching = useSelector((state: RootState) => state.inventory.fetching);
  const inventory = useSelector(selectors.selectInventory);
  const dispatch = useDispatch();
  const [dataLoaded, setDataLoaded] = useState(false);
  useEffect(() => {
    const unsubscribe = props.navigation.addListener("focus", () => {
      dispatch(actions.fetchInventory());
    });
    return unsubscribe;
  }, [props.navigation]);

  useEffect(() => {
    setDataLoaded(inventory && inventory.length > 0);
  }, [inventory]);

  return (
    <View style={{ position: "relative" }}>
      <Appbar.Header style={styles.header}>
        <Appbar.Content style={styles.headerTitle} title="Inventory" />
      </Appbar.Header>
      <View style={styles.container}>
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={fetching}
              onRefresh={() => dispatch(actions.fetchInventory())}
            />
          }
        >
          {dataLoaded ? <InventoryList products={inventory} /> : null}
        </ScrollView>
      </View>
      <SafeAreaView style={styles.fab}>
        <FAB
          icon={() => (
            <MaterialCommunityIcons name="barcode" size={24} color="#0B5549" />
          )}
          label="Scan Product"
          onPress={() => props.navigation.navigate("Camera")}
        />
      </SafeAreaView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: Dimensions.get("window").height - 100,
    padding: 0,
    backgroundColor: "#fff"
  },
  fab: {
    position: "absolute",
    bottom: 34,
    width: "100%",
    alignItems: "center"
  },
  header: {
    height: 100
  },
  headerTitle: {
    alignItems: "center"
  }
});
