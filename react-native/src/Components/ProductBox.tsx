import { StyleSheet, Text, View } from "react-native";
import React from "react";
import { Inventory } from "../store/inventory";
import NewLabel from "./NewLabel";
import ProductImage from "./ProductImage";
import ProductDetails from "./ProductDetails";

interface Props {
  product: Inventory;
}

function monthDiff(dateFrom: Date, dateTo: Date) {
  var months;
  months = (dateTo.getFullYear() - dateFrom.getFullYear()) * 12;
  months -= dateFrom.getMonth();
  months += dateTo.getMonth();
  return months <= 0 ? 0 : months;
}

const isNew = (date: string): boolean => {
  var productDate = new Date(date);
  var now = new Date();
  return monthDiff(productDate, now) < 8;
};

const ProductBox = ({ product }: Props): JSX.Element => {
  return (
    <View style={[styles.container, styles.shadowProp]}>
      <View style={styles.columns}>
        <ProductImage url={product.fields["Product Image"]} />
        <ProductDetails
          title={product.fields["Product Name"]}
          date={product.fields.Posted}
          categories={product.fields["Product Categories"]}
        />
      </View>

      <View style={styles.stickToTopRight}>
        {isNew(product.fields.Posted) ? <NewLabel /> : null}
      </View>
    </View>
  );
};

export default ProductBox;

const styles = StyleSheet.create({
  container: {
    position: "relative",
    padding: 8,
    width: "100%",
    height: 128,
    backgroundColor: "#F8F9FC",
    marginBottom: 12,
    borderRadius: 4
  },
  columns: {
    display: "flex",
    flexDirection: "row",
    alignItems: "flex-start"
    // gap: "8px"
  },
  stickToTopRight: {
    position: "absolute",
    top: 8,
    right: 8
  },
  shadowProp: {
    shadowColor: "#182430",
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.25,
    shadowRadius: 3
  }
});
