import { StyleSheet, Text, View } from "react-native";
import React from "react";
import CategoryBox from "./CategoryBox";

interface Props {
  title?: string;
  date: string;
  categories?: string;
}

const formatDate = (date: string): string => {
  var date = new Date(date).toLocaleDateString();
  var dateFormat = require("dateformat");
  return dateFormat(date, "dd.mm.yyyy");
};

const formatName = (name?: string): string => {
  return name == undefined || name.length == 0 ? "Name unavailable" : name;
};

const formatCategories = (categories: string): string[] => {
  var cats = categories.replace(/en:/g, "").split(", ").join(",").split(",");
  return cats;
};
const ProductDetails = ({ title, date, categories }: Props): JSX.Element => {
  return (
    <View>
      <View style={styles.textContainer}>
        <Text
          textBreakStrategy="balanced"
          numberOfLines={1}
          style={styles.title}
        >
          {formatName(title)}
        </Text>
        <Text style={styles.date}>{formatDate(date)}</Text>
      </View>
      <View style={styles.categories}>
        {categories
          ? formatCategories(categories)
              .slice(0, 3)
              .map((category, index) => (
                <CategoryBox category={category} key={index} />
              ))
          : null}
      </View>
    </View>
  );
};

export default ProductDetails;

const styles = StyleSheet.create({
  title: {
    fontSize: 18,
    fontWeight: "800",
    color: "#1B2633"
  },
  date: {
    fontSize: 12,
    marginTop: 2
  },
  textContainer: {
    width: "60%"
  },
  categories: {
    width: 270,
    height: 120,
    display: "flex",
    flexDirection: "row",
    flexWrap: "wrap",
    marginTop: 12
  }
});
