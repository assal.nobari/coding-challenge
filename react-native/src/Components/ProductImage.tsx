import { StyleSheet, Image, View } from "react-native";
import React from "react";

interface Props {
  url?: string;
}
const ProductImage = ({ url }: Props) => {
  return (
    <View style={styles.container}>
      {url ? (
        <Image style={styles.image} source={{ uri: url }}></Image>
      ) : (
        <Image
          style={styles.icon}
          source={require("./Icons/imagePlaceHolder.png")}
        />
      )}
    </View>
  );
};

export default ProductImage;

const styles = StyleSheet.create({
  container: {
    width: 85,
    height: 112,
    justifyContent: "center",
    alignItems: "center",
    marginRight: 20
  },
  image: {
    width: 85,
    height: 112
  },
  icon: {
    width: 48,
    height: 48
  }
});
