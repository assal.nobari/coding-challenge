import { StyleSheet, Text, View } from "react-native";
import React from "react";

interface Props {
  category: string;
}

const CategoryBox = ({ category }: Props) => {
  return (
    <View style={styles.container}>
      <Text numberOfLines={1}>{category}</Text>
    </View>
  );
};

export default CategoryBox;

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#D4E5FF",
    minHeight: 26,
    paddingTop: 2,
    paddingBottom: 2,
    paddingRight: 12,
    paddingLeft: 12,
    borderRadius: 48,
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 8,
    marginRight: 2,
    maxWidth: 120
  }
});
