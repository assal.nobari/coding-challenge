import { StyleSheet, View } from "react-native";
import React from "react";
import { Inventory } from "../store/inventory";
import ProductBox from "./ProductBox";

interface Props {
  products: Inventory[];
}
const InventoryList = ({ products }: Props): JSX.Element => {
  return (
    <View style={styles.container}>
      {products.map((record, index) => (
        <ProductBox product={record} key={index} />
      ))}
    </View>
  );
};

export default InventoryList;

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center",
    padding: 16,
    backgroundColor: "#fff"
  }
});
