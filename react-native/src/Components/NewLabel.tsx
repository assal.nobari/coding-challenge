import { StyleSheet, Text, View } from "react-native";
import React from "react";

const NewLabel = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>NEW</Text>
    </View>
  );
};

export default NewLabel;

const styles = StyleSheet.create({
  container: {
    width: 53,
    height: 34,
    backgroundColor: "#333333",
    color: "#fff",
    fontSize: 14,
    justifyContent: "center",
    alignItems: "center",
    borderTopLeftRadius: 9,
    borderTopEndRadius: 0,
    borderBottomLeftRadius: 9,
    borderBottomEndRadius: 9
  },
  title: {
    fontSize: 14,
    color: "#fff"
  }
});
